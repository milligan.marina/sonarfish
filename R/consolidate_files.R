#' @title Consolidate Mark Data
#' @description Consolidates mark data into a single data array
#' @param input_folder folder containing files to consolidate
#' @details Built to compile mark data inside ".txt" files created using
#'   Echotastic software (version 3). This function consolidates mark data from
#'   files located within the input folder, adds collumn names, and adds and
#'   identifying collumn to each row containing source filename.
#' @export
consolidate_files <- function(input_folder) {
  #create directory of files inside path to newfolder
  sonar_paths <- dir(input_folder, full.names = TRUE)
  names(sonar_paths) <- basename(sonar_paths)
  #compile txt files
  f1 <- plyr::ldply(
    sonar_paths,
    read.table,
    header = FALSE,
    fill = TRUE,
    stringsAsFa6ctors = FALSE,
    col.names = c(
      "sample",
      "ping",
      "time",
      "range",
      "amplitude",
      "xangle",
      "yangle",
      "direction",
      "length",
      "area",
      "operator"
    )
  )
}

