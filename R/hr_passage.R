#' @title Estimate Hourly Passage
#' @description Estimate hourly passage using mark data and file time
#' @param data output from tidy_markdata_br() function
#' @param threshold mininum length of file recording (per scan range) required
#'   for hourly estimate
#' @importFrom dplyr "%>%"
#' @details  This function estimates hourly passage from mark data. The expanded
#'   number of targets for each hour (P) is calculated as:
#'
#'   P=(60/h)X
#'
#'   Where h is the number of minutes of file counted and X is the
#'   number of fish counted. P is rounded down to the nearest fish.

#' The threshold enables users to specify the minimum number of minutes of file
#' counted required to generate an hourly estimate. We recomended a threshold of
#' 10 minutes for two range sonar projects where sonar files last 30 minutes,
#' and 20 minutes for single range sonar projects where sonar is recorded for 60
#' minutes.
#'
#' This function also expands the array and creates a row for each hour (per
#' scan range) where no sonar files was recorded.
#'
#' Effort from multiple files recorded within the same scan range at the same
#' hour are additive.
#'
#' @export
hr_passage <- function(data, threshold = 10) {

  #Create unique IDEN collumn for data
 data$start <- lubridate::minute(lubridate::ymd_hms(data$date_time)) #minute of hour file started recording

 data$IDEN <-
    paste(data$bank, data$date_hour, data$scan_range, sep = "_")

#convert data to numeric
  data$direction <- as.numeric(paste(data$direction))
  data$mins <- as.numeric(paste(data$mins))

  ###summarise by IDEN and lenght of file (this extra step identifies cases where more than one file were recorded)
  x4 <-
    data %>%  plyr::mutate(direction2 = ifelse(is.na(direction), 0, direction)) %>%  ###turns na directions ino 0s
    dplyr::group_by(IDEN, start) %>%  ###sorts by Date hour and scan_range
    dplyr::summarize(
      mins=max(mins, na.rm=TRUE),
      Upstream_Marks = sum(direction > 0, na.rm = TRUE),
      Downstream_Marks = sum(direction < 0, na.rm = TRUE),
      Net_Upstream_Marks = sum(direction, na.rm = TRUE),
      operator = max(operator, na.rm = TRUE)
    )

  ###summarise by IDEN
  x5 <-
    x4 %>% dplyr::group_by(IDEN) %>%  ###sorts by Date hour and scan_range
    dplyr::filter(mins >= threshold) %>%   ###remove any files under threshold mins
    dplyr::summarize(
      mins = sum(mins),
      Upstream_Marks = sum(Upstream_Marks, na.rm = TRUE),
      Downstream_Marks = sum(Downstream_Marks, na.rm = TRUE),
      Net_Upstream_Marks = sum(Net_Upstream_Marks, na.rm = TRUE),
      operator = max(operator, na.rm = TRUE)
    )



  ###Estimate hourly count (rounded down)
  x5$Passage_Estimate=trunc(60/as.numeric(paste(x5$mins))*as.numeric(paste(x5$Net_Upstream_Marks)))###rounds counts towards 0

  #Find min/max times of enumeration
  Tails <- as.data.frame(data %>%  dplyr::group_by(bank) %>%
                           dplyr::summarize(
                             start = min
                             (date_hour, na.rm = TRUE),
                             end = max(date_hour, na.rm = TRUE)
                           ))
  banks <- levels(as.factor(paste(Tails$bank)))
  scan_ranges <- levels(as.factor(paste(data$scan_range)))

  all_hours = as.POSIXct(seq(Tails$start, Tails$end, by = "hours"))
  all_hours_exp <-
    as.data.frame(expand.grid(all_hours, scan_ranges, banks))

  names(all_hours_exp) <- c("date_hour", "scan_range", "banks")

  #Create unique ID collumn
  all_hours_exp$IDEN <-
    paste(all_hours_exp$banks,
          all_hours_exp$date_hour,
          all_hours_exp$scan_range,
          sep = "_")

  all_hours_exp2 <-
    dplyr::select(all_hours_exp, IDEN)### select collumns to join

  #Add missing files to count summary
  df <- dplyr::left_join(all_hours_exp2, x5, by = "IDEN")

  #break apart values in IDEN collumn
  df <- as.data.frame(df %>%
                        tidyr::separate(IDEN, c("bank", "date_hour", "scan_range"), sep =
                                          "_"))
  ##Add Date collumn
  df$Date <- lubridate::date(df$date_hour)

  ##Rearrange order of rows
  df <-
    df %>% dplyr::select(
      bank,
      Date,
      date_hour,
      scan_range,
      operator,
      mins,
      Upstream_Marks,
      Downstream_Marks,
      Net_Upstream_Marks,
      Passage_Estimate
    ) %>%
    dplyr::arrange(bank, Date, date_hour)
  }
