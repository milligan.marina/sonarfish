#' @title Houly Passage Estimate with Kalman Smoothing
#' @description Perform Kalman smoothing on hourly passage estimate
#' @param hr_passage_est hourly passage estimate from hr_passage()
#' @importFrom dplyr "%>%"
#' @details  Estimates passage during missing data gaps Kalman smoothing. The
#'   algorith is run on each bank and scan range combo seperately Uses the
#'   na.kalman() function from the ImputeTS package. Default use is Structural
#'   models (StructTS).
#' @export
impute_kalman <- function(hr_passage_est) {
  df <-
  hr_passage_est %>% dplyr::mutate(ID = paste(bank, scan_range))
  cats <- levels(as.factor(paste(df$ID)))
  p <- list()

  for (i in cats) {
    p[[i]] <-
      df %>% dplyr::filter(ID == i) %>% dplyr::arrange(date_hour) %>% dplyr::mutate(
        Passage_Estimate = imputeTS::na.kalman(Passage_Estimate, model = "StructTS", smooth =
                                                 TRUE)
      )
  }
  df2 <-  do.call(rbind, p)
  df3 <-df2%>% dplyr::select(c(1:6,9:10))
}
