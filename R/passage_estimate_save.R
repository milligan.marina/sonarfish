#' @title Save Hourly Passage Data
#' @description Save hourly passage estimate t0 .csv
#' @param imputed_data hourly passage estimate after imputation
#' @param output_filename name of output folder.  Default is "Processed_Sonar"
#' @param project_name specify project name
#' @importFrom dplyr "%>%"
#' @details Creates an output folder (default name is "Processed_Sonar") inside
#'   the working directory and writes a .csv file into a subfolder named
#'   "Hourly Data".  The .csv files name includes the last date of
#'   recorded sonar, project name and information

#' @export
passage_estimate_save <-

  function(imputed_data,
           output_filename = "Processed_Sonar",
           project_name) {

    #Identify last day of sonar (for OUTPUT name)
    lastDate <- max(imputed_data$Date, na.rm = TRUE)
    lastDate <- lubridate::date(lastDate)

    #Create Output directory to copy files to
    FolderName <-
      paste(getwd(), output_filename, "Hourly_Data/", sep = "/")
    dir.create(FolderName, recursive = TRUE)# Make sure the directory exists

    pathout <- paste(FolderName)
    FileName <-
      paste(pathout,
            lastDate,
            "_",
            project_name,
            "_",
            "Hourly.Data",
            ".csv" ,
            sep = "")  ###specifies order of file name
    write.csv(imputed_data,  file = FileName)  ###writes file

  }
