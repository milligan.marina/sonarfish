#' @title sonar_VAR2
#' @description Calculates variance for projects using 2 or fewer sonar ranges
#' @param hourData hourly data file output from hr_passage()
#' @return VAR variance
#' @details Must add a "Species" collumn to the hourly data in order to work.
#'   Appropriate for one or two range sonar projects. Equation is explained in
#'   detail in DFO (2019) and McDougalland Lozori(2018).
#' @export
sonar_VAR2 <- function(hourData) {
  df2 <-
    hourData %>% dplyr::mutate(A = 1) %>%  # Creates collumn A which is used to summarize the number of sonar files counted each day in the next step
    dplyr::mutate(Passage_Estimate = ifelse(is.na(Net_Upstream_Marks), "", Passage_Estimate)) #remove impuations

  df5 <- df2 %>%
    dplyr::group_by(Species, bank, scan_range, Date = ymd(Date)) %>% # Organizes data by Species,Shore
    dplyr::summarise(n = sum(A), minutes = sum(mins, na.rm = TRUE))# Calculates daily  n and minutes.

  df3 <- df2 %>%
    dplyr::group_by(Species, bank, scan_range) %>% # Organizes data by Shore
    dplyr::arrange(ymd(Date), date_hour) %>% # Makes sure data is in order by date time
    dplyr::mutate(X = type.convert(paste(Passage_Estimate)) / (mins / 60)) %>% # Uses count and fraction of hour sampled to calculate hourly passage  or Ydsp/hdsp (X)
    dplyr::mutate(Y = ifelse(is.na(mins), NA, lag(X, default = 0))) %>% # Calculates Ydf,p-1/hds,p-1 (Y) as lag(x)
    dplyr::mutate(Z = (X - Y) ^ 2)# If X or Y are NAs they are removed

  df6 <- df3 %>%
    dplyr::group_by(Species, bank, scan_range, Date = ymd(Date)) %>%
    dplyr::summarise(daily.SUM = sum(Z, na.rm = TRUE))# Calculates daily Z


  dat <-
    dplyr::full_join(df6, df5, by = c("Species", "bank", "scan_range", "Date"))
  dat2 <- dat %>% dplyr::group_by(Species, bank) %>%
    dplyr::mutate(Var = (daily.SUM / (2 * (n - 1)) * ((1 - ((minutes / 60) /
                                                              24
    )) / n) * 24 ^ 2)) # Calculates daily Inshore Variance

  VAR <- dat2 %>%
    dplyr::group_by(Species, Bank = bank, scan_range) %>%
    dplyr::summarize(VAR = sum(Var, na.rm = TRUE))
  print(VAR)
}
